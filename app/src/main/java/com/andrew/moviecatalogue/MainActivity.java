package com.andrew.moviecatalogue;

import android.content.Intent;
import android.content.res.TypedArray;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private TypedArray dataPoster;
    private String[] dataTitle, dataSynopsis, dataGenre, dataRating, dataReleaseDate;
    private MovieAdapter adapter;
    private ArrayList<Movie> movies;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        adapter = new MovieAdapter(this);
        prepare();
        addItem();
        ListView listView = findViewById(R.id.lv_movie);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent movieDetailIntent = new Intent(MainActivity.this, MovieDetail.class);
                movieDetailIntent.putExtra(MovieDetail.EXTRA_MOVIE, movies.get(i));
                startActivity(movieDetailIntent);
            }
        });
    }

    private void prepare() {
        dataPoster = getResources().obtainTypedArray(R.array.data_poster);
        dataTitle = getResources().getStringArray(R.array.data_title);
        dataSynopsis = getResources().getStringArray(R.array.data_synopsis);
        dataGenre = getResources().getStringArray(R.array.data_genre);
        dataRating = getResources().getStringArray(R.array.data_rating);
        dataReleaseDate = getResources().getStringArray(R.array.data_release_date);
    }

    private void addItem() {
        movies = new ArrayList<>();

        for (int i = 0; i < dataTitle.length; i++) {
            Movie movie =  new Movie();
            movie.setPoster(dataPoster.getResourceId(i, -1));
            movie.setTitle(dataTitle[i]);
            movie.setSynopsis(dataSynopsis[i]);
            movie.setGenre(dataGenre[i]);
            movie.setRating(dataRating[i] + " / 10");
            movie.setRelease_date(dataReleaseDate[i]);
            movies.add(movie);
        }

        adapter.setMovies(movies);

    }

}
