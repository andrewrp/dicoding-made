package com.andrew.moviecatalogue;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

public class MovieDetail extends AppCompatActivity {

    public static final String EXTRA_MOVIE = "extra_movie";
    ImageView imgPoster;
    TextView tvTitle, tvGenre, tvReleaseDate, tvRating, tvSynopsis;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);

        imgPoster = findViewById(R.id.img_poster);
        tvTitle = findViewById(R.id.tv_title);
        tvGenre = findViewById(R.id.tv_genre);
        tvReleaseDate = findViewById(R.id.tv_release_date);
        tvRating = findViewById(R.id.tv_rating);
        tvSynopsis = findViewById(R.id.tv_detail_synopsis);

        Movie movie = getIntent().getParcelableExtra(EXTRA_MOVIE);
        imgPoster.setImageResource(movie.getPoster());
        tvTitle.setText(movie.getTitle());
        tvGenre.setText(movie.getGenre());
        tvReleaseDate.setText(movie.getRelease_date());
        tvRating.setText(movie.getRating());
        tvSynopsis.setText(movie.getSynopsis());

        getSupportActionBar().setTitle(movie.getTitle());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
